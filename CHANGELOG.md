# Changelog

- V1.0.1 - Fixed typo in changelog
- V1.0.0 - Most simple examples
  - Output a beep sound
  - Plot samples from microphone
  - Minimum component count
  - No additional dependencies
    - Only uses Arduino buit-in library

