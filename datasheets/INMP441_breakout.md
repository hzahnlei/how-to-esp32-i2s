# INMP441 Breakout Board

I am providing my own "datasheet", as I cound not find a data sheet for the INMP441 breakout board.

```
        /-----------\
       /             \
L/R -- !o           o! -- GND
       !             !
 WS -- !o     O     o! -- VDD
       !             !
SCK -- !o           o! -- SD
       \             /
        \-----------/
```

| PIN | Description |
| --- | ----------- |
| L/R | I<sup>2</sup> selects whether microphone outputs to left or right channel |
| WS  | I<sup>2</sup> select data word format |
| SCK | I<sup>2</sup> data clock |
| SD  | I<sup>2</sup> digital audio output |
| VDD | Power supply, 1.62V - 3.63V |
| GND | Ground |

The microphone is operated with 3.3V microcontrollers very easily, as its maximum supply voltage is 3.63V.
Microcontroller boards usually feature a supply voltage pin that can be used to power the microphone.
