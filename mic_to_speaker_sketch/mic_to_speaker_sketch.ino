/*
 * Holger Zahnleiter, 2021-11-15
 */

#include "driver/i2s.h"

namespace microphone
{
        static constexpr i2s_config_t I2S_CONFIG{.mode = (i2s_mode_t)(I2S_MODE_MASTER | I2S_MODE_RX),
                                                 .sample_rate = 44100,
                                                 .bits_per_sample = I2S_BITS_PER_SAMPLE_16BIT,
                                                 .channel_format = I2S_CHANNEL_FMT_ONLY_LEFT,
                                                 .communication_format =
                                                     (i2s_comm_format_t)(I2S_COMM_FORMAT_I2S | I2S_COMM_FORMAT_I2S_MSB),
                                                 .intr_alloc_flags = ESP_INTR_FLAG_LEVEL1, // high interrupt priority
                                                 .dma_buf_count = 8,                       // 8 buffers
                                                 .dma_buf_len = 1024, // 1K per buffer, so 8K of buffer space
                                                 .use_apll = 0,
                                                 .tx_desc_auto_clear = true,
                                                 .fixed_mclk = -1};

        static constexpr i2s_pin_config_t I2S_PIN_CONFIG{
            .bck_io_num = 19,                  // I2S bit clock
            .ws_io_num = 18,                   // I2S word select (L/R)
            .data_out_num = I2S_PIN_NO_CHANGE, // No I2S data out this example
            .data_in_num = 17                  // I2S data out
        };

        static constexpr auto I2S_PORT = I2S_NUM_1;
} // namespace microphone

namespace speaker
{
        static constexpr i2s_config_t I2S_CONFIG{.mode = (i2s_mode_t)(I2S_MODE_MASTER | I2S_MODE_TX),
                                                 .sample_rate = 44100,
                                                 .bits_per_sample = I2S_BITS_PER_SAMPLE_16BIT,
                                                 .channel_format = I2S_CHANNEL_FMT_ONLY_LEFT,
                                                 .communication_format =
                                                     (i2s_comm_format_t)(I2S_COMM_FORMAT_I2S | I2S_COMM_FORMAT_I2S_MSB),
                                                 .intr_alloc_flags = ESP_INTR_FLAG_LEVEL1, // high interrupt priority
                                                 .dma_buf_count = 8,                       // 8 buffers
                                                 .dma_buf_len = 1024, // 1K per buffer, so 8K of buffer space
                                                 .use_apll = 0,
                                                 .tx_desc_auto_clear = true,
                                                 .fixed_mclk = -1};

        static constexpr i2s_pin_config_t I2S_PIN_CONFIG{
            .bck_io_num = 27,                // I2S bit clock
            .ws_io_num = 26,                 // I2S word select (L/R)
            .data_out_num = 25,              // I2S data out
            .data_in_num = I2S_PIN_NO_CHANGE // No I2S data in this example
        };

        static constexpr auto I2S_PORT = I2S_NUM_0;
} // namespace speaker

auto setup() -> void
{
        i2s_driver_install(microphone::I2S_PORT, &microphone::I2S_CONFIG, 0, nullptr);
        i2s_set_pin(microphone::I2S_PORT, &microphone::I2S_PIN_CONFIG);
        i2s_driver_install(speaker::I2S_PORT, &speaker::I2S_CONFIG, 0, nullptr);
        i2s_set_pin(speaker::I2S_PORT, &speaker::I2S_PIN_CONFIG);
}

using Wave_Data = int16_t;

static constexpr auto INPUT_BUFFER_SIZE = 100U;
static constexpr auto MAX_BYTES_TO_READ = INPUT_BUFFER_SIZE * sizeof(Wave_Data);
static Wave_Data input_buffer[INPUT_BUFFER_SIZE];
static size_t bytes_read;
static size_t bytes_written;

auto loop() -> void
{
        i2s_read(microphone::I2S_PORT, &input_buffer, MAX_BYTES_TO_READ, &bytes_read, portMAX_DELAY);
        if (bytes_read > 0)
        {
                i2s_write(speaker::I2S_PORT, &input_buffer, bytes_read, &bytes_written, portMAX_DELAY);
        }
}
