# How to: Use I<sup>2</sup>S on ESP32 (Arduino Libray)

Simple project to learn how to use I<sup>2</sup>S on ESP32. Uses MAX98357A mono amp and an INMP441 MEMS microphone.

Actually, I do not want to learn about I<sup>2</sup>S on ESP32.
I had MAX98357A and INMP441 breakout boards laying around for some time and wanted to see how these work.

For a quick guide to ESP32 programming with Arduino IDE see https://gitlab.com/hzahnlei/how-to-esp32-arduino.

## Prerequistes

- Arduino IDE 1.8.16 (https://www.arduino.cc/en/software)
- Arduino ESP32 add-on from Espressif (https://dl.espressif.com/dl/package_esp32_index.jso)
- NodeMCU ESP32 module
- MAX98357A breakout board (Adafruit in my case)
- INMP441 breakout board (unbranded in my case)
- Micro-USB cable
- Breadboard
- Jumper wires
- Small speaker, 4 Ohm or 8 Ohm

## Most simple example: Beep

This will only generate a continuous beep.
No prepared WAV file nor any SD card required.
This gives us the most simple hardware set-up and example program.

Attach the speaker to the MAX98357A breakout board.
Connect the MAX98357A breakout board to the NodeMCU ESP32 like so:

<img src="./beep_wiring.jpeg"/>

| NodeMCU pin | MAX98357A pin | Description |
| ----------- | ------------- | ----------- |
| GND         | GND           | Ground      |
| 3.3V        | Vin           | The microcontroller powers the amp with 3.3V. The amp can also take 5.0V and would then be louder, if this is what you want. |
| G25         | DIN           | I<sup>2</sup>S data input line |
| G26         | LRC           | Select left/right I<sup>2</sup>S channel. However, the MAX98357A is mono and mixes both channels together. |
| G27         | BCLK          | I<sup>2</sup>S bus clock |

In the real world it should look something like this:

<img src="./beep_breadboard.jpeg"/>

Now compile and upload `beep_sketch/beep_sketch.ino`.
You shoud now hear a continuous beeping sound.

## Also simple: Plotting Samples From the Microphone

Another simple example.
Simple in that no additional library is required.
Use Arduino IDE's serial plotter to visualize the audio data captured by the microphone.
This should look something like this.

<img src="./plotting_audio_samples.png"/>

But before, wire up ESP32 and microphone like shown below.
Compile and upload `mic_plot_sketch/mic_plot_sketch.ino`.

<img src="./mic_plot_wiring.jpeg"/>

| NodeMCU pin | INMP441 pin | Description |
| ----------- | ----------- | ----------- |
| GND         | GND         | Ground      |
| 3.3V        | VDD         | The microcontroller powers the mic with 3.3V. The mic cannot take 5.0V. |
| G17         | SD          | I<sup>2</sup>S data output line |
| G18         | WS          | I<sup>2</sup>S word select |
| G19         | SCK         | I<sup>2</sup>S bus clock |

On a breadboard it should look something like this:

<img src="./mic_plot_breadboard.jpeg"/>

## Directly Sending Audio From Microphone to Speaker

In our last experiment, we will read audio data from the microphone and send it directly to the amplifier.

For this, wire up ESP32, microphone and amp/speaker like so:

<img src="./mic_to_speaker_wiring.jpeg"/>

<img src="./mic_to_speaker_breadboard.jpeg"/>

Compile and upload `mic_to_speaker_sketch/mic_to_speaker_sketch.ino`.
Now, the speaker should output all noises captured by the microphone.
The speaker is located close to the microphone.
So expect some feedback, which sounds like a short delay.

The code is rather brutish.
Not really a role model.
I am just reading samples from the microphone (I<sup>2</sup>S port #1) into a small buffer.
This buffer is then immediately output to the speaker (I<sup>2</sup>S port #0).
At least this example code does not require any additional I<sup>2</sup>S libraries.
Like all examples herein it only employs the basic I<sup>2</sup>S ESP32 core for Arduino.

Notice the I<sup>2</sup>S configuration part of the program.
Input and output are configured to use one channel only (left), as there is only one microphone attached.
With two microphones we could use both channels to implement a stereo microphone.

## Credits

Last but not least

- My experiments rely heavily on this article series by XTronical: https://www.xtronical.com/i2s-ep1/
- Here is another helpful tutorial I recommend: https://diyi0t.com/i2s-sound-tutorial-for-esp32/
