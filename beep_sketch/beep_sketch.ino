/*
 * This is a slightly modified version of XTronical's original.
 *
 * Holger Zahnleiter, 2021-11-11
 *
 * Boring copyright/usage information:
 *    (c) XTronical, www.xtronical.com
 *    Use as you wish for personal or monatary gain, or to rule the world (if that sort of thing spins your bottle)
 *    However you use it, no warrenty is provided etc. etc. It is not listed as fit for any purpose you perceive
 *    It may damage your house, steal your lover, drink your beers and more.
 */

#include "driver/i2s.h"

static constexpr i2s_config_t i2s_config{.mode = (i2s_mode_t)(I2S_MODE_MASTER | I2S_MODE_TX),
                                         .sample_rate = 44100,
                                         .bits_per_sample = I2S_BITS_PER_SAMPLE_16BIT,
                                         .channel_format = I2S_CHANNEL_FMT_RIGHT_LEFT,
                                         .communication_format =
                                             (i2s_comm_format_t)(I2S_COMM_FORMAT_I2S | I2S_COMM_FORMAT_I2S_MSB),
                                         .intr_alloc_flags = ESP_INTR_FLAG_LEVEL1, // high interrupt priority
                                         .dma_buf_count = 8,                       // 8 buffers
                                         .dma_buf_len = 1024, // 1K per buffer, so 8K of buffer space
                                         .use_apll = 0,
                                         .tx_desc_auto_clear = true,
                                         .fixed_mclk = -1};

static constexpr i2s_pin_config_t pin_config{
    .bck_io_num = 27,                // I2S bit clock
    .ws_io_num = 26,                 // I2S word select (L/R)
    .data_out_num = 25,              // I2S data out
    .data_in_num = I2S_PIN_NO_CHANGE // No I2S data in this example
};

static constexpr i2s_port_t i2s_num = I2S_NUM_0;

auto setup() -> void
{
        i2s_driver_install(i2s_num, &i2s_config, 0, nullptr);
        i2s_set_pin(i2s_num, &pin_config);
}

using Volume = uint16_t;
using Wave_Data = int16_t;
using I2S_Data = uint32_t;
using Time = uint16_t;

/*
 * This creates the 32 bit word we send to the I2S interface/buffers.
 * 16 bits for the righ channel and 16 bits for the left.
 * As we are sending the same wave to both channels we just combine them using some bit shifting and masking.
 */
[[nodiscard]] inline auto on_both_channels(const Wave_Data mono_value) -> I2S_Data
{
        return (mono_value << 16) | (mono_value & 0xffff);
}

[[nodiscard]] inline auto half_wave_is_elapsed(const Time curr_time) -> bool
{
        return 0 == curr_time;
}

static constexpr Volume MAX_VOLUME = 0x3ff;
static constexpr Wave_Data MAX_PEAK_VOLUME = MAX_VOLUME;
static constexpr Wave_Data MIN_THROUGH_VOLUME = -MAX_VOLUME;

inline auto toggle(Wave_Data &output_value) -> void
{
        output_value = (MAX_PEAK_VOLUME == output_value) ? MIN_THROUGH_VOLUME : MAX_PEAK_VOLUME;
}

static constexpr Time WAVE_LENGTH = 50;

inline auto reset(Time &curr_time) -> void
{
        curr_time = WAVE_LENGTH;
}

inline auto progress(Time &curr_time) -> void
{
        curr_time--;
}

inline auto output_to_i2s(I2S_Data audio_data) -> void
{
        size_t bytes_written;
        i2s_write(i2s_num, &audio_data, 4, &bytes_written, portMAX_DELAY);
}

static Time curr_time = WAVE_LENGTH;
static Wave_Data curr_output_value = MAX_PEAK_VOLUME;

/*
 * Generates a square wave.
 */
auto loop() -> void
{
        if (half_wave_is_elapsed(curr_time))
        {
                toggle(curr_output_value);
                reset(curr_time);
        }
        progress(curr_time);
        output_to_i2s(on_both_channels(curr_output_value));
}
